drop PROCEDURE if exists Ingresarvendedor;
 CREATE PROCEDURE  Ingresarvendedor(
in p_codigo varchar(50),
 in p_nombre varchar(100))
BEGIN
INSERT into vendedor values( p_codigo, p_nombre);
END;
drop PROCEDURE if exists modificarvendedor;
 CREATE PROCEDURE  modificarvendedor(
in p_codigo varchar(50),
 in p_nombre varchar(100)
 )
BEGIN
update vendedor SET
  nombre=p_nombre
  where codigo=p_codigo;
END;

drop PROCEDURE if exists borrarrvendedor;
 CREATE PROCEDURE  borrarrvendedor(
in p_codigo varchar(50))
BEGIN
delete from vendedor WHERE codigo=p_codigo;
END;